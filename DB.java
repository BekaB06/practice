package com;

import java.sql.*;

public class DB {
    private static Connection connection;

    private DB() {
    }

    public static Connection getConnection() {

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Simple_DB","postgres", "0604");
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return connection;
    }
}
