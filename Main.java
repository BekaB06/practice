package com;

public class Main {
    public void main(String[] args) {
        Employee employee = new Employee();
        Manager manager = new Manager();
        DBOperations db = new DBOperations();
        db.insert(employee);
        Stuff st = new Stuff();
        st.addEmp(new JavaDeveloper("Bek","Baktygereev","Astana","$50,000"));
        st.addEmp(new Manager("Mike","Angelo","USA","$100,000"));

        st.Work();
    }
}
