package com;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBOperations {
    public void insert(Employee employee){
      String insertSql = "INSERT INTO employees (id, name, surname, email, phone)";
        try {
            PreparedStatement stmt = DB.getConnection().prepareStatement(insertSql);

            stmt.setInt(1,employee.getID());
            stmt.setString(2,employee.getName());
            stmt.setString(3,employee.getSurname());
            stmt.setString(4,employee.getEmail());
            stmt.setString(5,employee.getPhone());
            stmt.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
